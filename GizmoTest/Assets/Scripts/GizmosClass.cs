﻿using UnityEngine;

public class GizmosClass : MonoBehaviour
{
    private Vector3 Center = Vector3.zero;
    private Vector3 Size = Vector3.one;
    private float Radius = 1f;
    private Vector3 From = Vector3.zero;
    private Vector3 Direction = Vector3.up;

    private Vector3 Start = new Vector3(1f, 0f, 0f);
    private Vector3 End = new Vector3(0f, 0f, 1f);

    private Rect screenRect = new Rect(0f, 0f, 100f, 100f);
    public Texture SampleTexture;

    private float fov = 60f;
    private float maxRange = 1f;
    private float minRange = 3f;
    private float aspect = 1.3f;

    private void OnDrawGizmos()
    {
        //Gizmos.DrawCube(Center, Size);
        //Gizmos.DrawWireCube(Center, Size);
        //Gizmos.DrawSphere(Center, Radius);
        //Gizmos.DrawWireSphere(Center, Radius);
        //Gizmos.DrawRay(From, Direction);
        //Gizmos.DrawLine(Start, End);
        //Gizmos.DrawIcon(transform.position, "GoalFlagGizmo.png");

        //if (SampleTexture != null)
        //{
        //    Gizmos.DrawGUITexture(screenRect, SampleTexture);
        //}

        Gizmos.DrawFrustum(Center, fov, maxRange, minRange, aspect);
    }
}
